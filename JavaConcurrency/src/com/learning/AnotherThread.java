/**
 * 
 */
package com.learning;

/**
 * @author durgeshsingh
 *
 */
public class AnotherThread extends Thread {

	@Override
	public void run() {
		System.out.println(ThreadColour.ANSI_BLUE + "Hello from " + currentThread().getName());
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.err.println(ThreadColour.ANSI_BLUE + "Another Thread Woke me up");
			return;
		}
		
		System.out.println(ThreadColour.ANSI_BLUE + "3 seconds has passed and I am awake again.");
	}
	
}
