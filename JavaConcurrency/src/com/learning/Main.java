/**
 * 
 */
package com.learning;

/**
 * @author durgeshsingh
 *
 */
public class Main {

	public static void main(String[] args) {
		System.out.println(ThreadColour.ANSI_PURPLE + "Hello from the main Thread.");

		// Instance from the child class
		Thread anotherThread = new AnotherThread();
		anotherThread.setName("== Another Thread ==");

		anotherThread.start();

		// Anonymous class
		new Thread() {
			@Override
			public void run() {
				System.out.println(ThreadColour.ANSI_GREEN + "Hello from the anonymous thread class");
			}
		}.start();

		Thread myRunnableThread = new Thread(new MyRunnable() {
			@Override
			public void run() {
				System.out.println(ThreadColour.ANSI_RED + "Hello from the anonymous class's implementation of run()");
				try {
					anotherThread.join();
					System.out.println(ThreadColour.ANSI_RED + "AnotherThread terminated, or timed out, So I am running again");
				} catch (InterruptedException e) {
					System.out.println(ThreadColour.ANSI_RED + "I couldn't wait after all. I was interrupted");
				}
			}
		});

		myRunnableThread.start();

		// Interrupting the thread
//		anotherThread.interrupt();

		System.out.println(ThreadColour.ANSI_PURPLE + "Hello Again from the main thread");
	}

}
