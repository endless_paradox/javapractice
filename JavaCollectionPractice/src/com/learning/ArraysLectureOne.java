/**
 * 
 */
package com.learning;

/**
 * @author durgeshsingh
 *
 */
public class ArraysLectureOne {
	
	public static void main(String[] args) {
		
		int[] myIntArray = new int[10];
		
		myIntArray[5] = 50;
		
		double[] myDoubleArray = new double[10];
		
		System.out.println(myIntArray[5]);
		
	}
	
}
